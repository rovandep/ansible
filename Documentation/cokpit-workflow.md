# Cockpit 

Cockpit makes GNU/Linux discoverable. See your server in a web browser and perform system tasks with a mouse. It’s easy to start containers, administer storage, configure networks, and inspect logs.

[Cockpit project](https://cockpit-project.org/)

## Business Use case: cockpit with master dashboard and multiple members 
In a standard way, the installation of cockpit is done on each machine and
each machine have the dashboard running without any concept of central access.

However, it is possible to elect a cockpit machine as a central dashboard server and register machines as members and administer these machine from an unique point of entry.

## Technical Use case

To demonstrate the capabilities of CloudForms to utilize Ansible playbooks and seamless integration of custom button and Ansible Tower triggered by a single custom button from the "Tools" menu on a single Virtual Machine or Instance. 

## Workflow Process

Click "Tools" button from a "powered on" Virutal Machine or Instance
    Click "Cockpit" button to open deployment form
        Select radio button "Cockpit member" or "Cockpit dashboard"
            if "Cockpit member"
                select a Cockpit dashboard machine from list of tagged Virtual Machine or Instances as "Cockpit dashboard" to register the member to
                    Click "Confirm"
                        Install cockpit service
                        Start cockpit service
                        Register member to selected dashboard
            if "Cockpit dashboard"
                show list of existing Cockpit dashboard to inform of existing ones
                    Click "Confirm"

## Workflow Implementation

- CloudForms 
    Create a Ansible Repository to access the playbook 
    Create a datastore with the related git repo
    Create Automation/Ansible Credentials
        Create a Configuration namespace 
        Create a "ConfigureCockpit" automate class
            Add Schema items:
                DeployCockpit
                TagAsDashboard
                RegisterToDashboard
        Create a "Configure" instance
        Create the following methods:
            cockpit_join: ansible - enable-cockpit-member.yaml
            deploy_cockpit: ansible - enable-cockpit.yaml
            tag_vm: ruby to tagg vm as CockpitMaster


            



- Ansible Tower
    Create a Project to map to the git repo
    Create 2 templates
        1 to deploy the client
        1 to deploy the server



- Tagging 



                                        CloudForms      
                                        VM/Instance  
                                        |Button|
                                        -decision-
                    <member>                                    <dashboard>
                    -Master List-                               > enable-cockpit-dashboard.yaml
                    > enable-cockpit-member.yaml                    install packages
                    > enable-cockpit-member.yaml                    tag as master 
                        install packages
                        register to master

                                            done
                                            \o/

