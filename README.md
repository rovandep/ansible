# README

This repository is a curated fork from the MBU Lab done during the RHTE 2019 HCM Hackathon. 

Contributors: Cedric Parent/Romuald Vandepoel from Red Hat Benelux Consulting

## WORK IN PROGRESS

The content in this repository is still in Work in Progress state and should be used with caution. Please report any issues by creating an Issue in this repository.

## Use cases

The individual demo's and use cases are documented in more details in the [Documentation](./Documentation/) sub folder.

## How to use this Demo

Although the code in the repository is released under the terms of the [GPL](./LICENSE), the lab environment can only be deployed by Red Hat employees or Partners.